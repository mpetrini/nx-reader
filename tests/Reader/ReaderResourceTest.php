<?php

namespace Reader;

use NXReader\Reader\ReaderResource;
use PHPUnit\Framework\TestCase;

class ReaderResourceTest extends TestCase
{
    private ReaderResource $readerResource;

    public function setUp(): void
    {
        $this->readerResource = new ReaderResource();
    }

    public function testOpen(): void
    {
        $this->assertTrue($this->readerResource->open(__DIR__.'/../data/CACTOT06500.txt'));
    }
}
