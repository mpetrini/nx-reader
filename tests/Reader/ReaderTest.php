<?php

namespace NXReader\Tests\Reader;

use NXReader\Ccam\Header;
use NXReader\Proxy\CcamProxy;
use NXReader\Reader\Reader;
use NXReader\Reader\ReaderResource;
use PHPUnit\Framework\TestCase;

class ReaderTest extends TestCase
{
    public function testNextRecord(): void
    {
        $readerResource = $this->createMock(ReaderResource::class);
        $readerResource->method('getNextData')->willReturn(
            '000  00000001000000CODCAMCT00000001000000CCAM  NACAC20200917                        000731140106500                             '
        );

        $reader = new Reader(
            new CcamProxy(),
            $readerResource
        );

        $entry = $reader->nextRecord();

        $this->assertInstanceOf(Header::class, $entry);
    }
}
