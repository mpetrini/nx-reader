<?php

namespace Ccam;

use DateTimeInterface;
use NXReader\Ccam\ModeTraitement;
use PHPUnit\Framework\TestCase;

class ModeTraitementTest extends TestCase
{
    public function test(): void
    {
        $entry = new ModeTraitement(
            '0101032005032500000000042005032500000000052005032500000000072007122800000000102007122800000000000000000000000000             '
        );

        $values = $entry->read();
        $this->assertArrayHasKey('dateDebut1', $values);
        $this->assertInstanceOf(DateTimeInterface::class, $values['dateDebut1']);
    }
}
