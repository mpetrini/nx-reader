<?php

namespace Ccam;

use NXReader\Ccam\Header;
use PHPUnit\Framework\TestCase;

class HeaderTest extends TestCase
{
    public function test(): void
    {
        $header = new Header(
            '  00000001000000CODCAMCT00000001000000CCAM  NACAC20200917                        000731140106500                             '
        );

        $values = $header->read();
        $this->assertArrayHasKey('numeroDestinataire', $values);
        $this->assertEquals('00000001000000', $values['numeroDestinataire']);
    }
}
