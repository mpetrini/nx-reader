<?php

namespace DBFReader\Tests;

use NXReader\Extractor\Extractor;
use NXReader\FileLoader\FileLoaderInterface;
use NXReader\Request\CurlRequest;
use NXReader\Request\HttpRequest;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class ExtractorTest extends TestCase
{
    public function testGetFile(): void
    {
        $path = __DIR__.'/../data/dbf_reader.zip';
        $fileLoader = $this->createMock(FileLoaderInterface::class);
        $fileLoader->method('getUrl')->willReturn('https://www.ameli.fr/fileadmin/user_upload/documents/CACTOT06500.zip');

        $httpRequest = $this->createMock(HttpRequest::class);
        $httpRequest->method('getInfo')->with(CURLINFO_HTTP_CODE)->willReturn(Response::HTTP_OK);
        $httpRequest->method('execute')->willReturn($path);

        $extractor = new Extractor($fileLoader, $httpRequest);

        $this->assertNotEmpty($extractor->getFile());
    }

    public function testUnzip(): void
    {
        $extractor = new Extractor(
            $this->createMock(FileLoaderInterface::class),
            $this->createMock(HttpRequest::class)
        );

        $this->assertNotNull($extractor->unzip(__DIR__.'/../data/dbf_reader.zip'));
    }
}
