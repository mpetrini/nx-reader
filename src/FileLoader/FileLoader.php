<?php

namespace NXReader\FileLoader;

class FileLoader implements FileLoaderInterface
{
    protected ?int $version = null;

    public function getUrl(): string
    {
        return 'https://www.ameli.fr/fileadmin/user_upload/documents/CACTOT0'.$this->version.'00.zip';
    }

    /**
     * @return int|null
     */
    public function getVersion(): ?int
    {
        return $this->version;
    }

    /**
     * @param int|null $version
     * @return FileLoader
     */
    public function setVersion(?int $version): FileLoader
    {
        $this->version = $version;
        return $this;
    }
}
