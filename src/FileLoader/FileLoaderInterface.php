<?php

namespace NXReader\FileLoader;

interface FileLoaderInterface
{
    public function getUrl(): string;
}
