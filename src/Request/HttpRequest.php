<?php
/**
 * Inspired from https://stackoverflow.com/questions/7911535/how-to-unit-test-curl-call-in-php
 */

namespace NXReader\Request;

interface HttpRequest
{
    public function init(): void;
    public function setOption($name, $value);
    public function execute();
    public function getInfo($name);
    public function close();
}
