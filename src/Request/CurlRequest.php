<?php
/**
 * Inspired from https://stackoverflow.com/questions/7911535/how-to-unit-test-curl-call-in-php
 */

namespace NXReader\Request;

class CurlRequest implements HttpRequest
{
    /**
     * @var ?resource
     */
    private $handle = null;

    public function init(): void
    {
        $this->handle = curl_init();
    }

    public function setOption($name, $value) {
        curl_setopt($this->handle, $name, $value);
    }

    public function execute() {
        return curl_exec($this->handle);
    }

    public function getInfo($name) {
        return curl_getinfo($this->handle, $name);
    }

    public function close() {
        curl_close($this->handle);
    }
}
