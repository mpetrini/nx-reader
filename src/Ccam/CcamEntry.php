<?php

namespace NXReader\Ccam;

use NXReader\Entry\Entry;

abstract class CcamEntry extends Entry
{
    abstract protected function getAlias(): string;
}
