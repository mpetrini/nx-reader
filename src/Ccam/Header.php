<?php

namespace NXReader\Ccam;

use NXReader\Entry\Entry;
use NXReader\Entry\StringColumn;
use NXReader\Entry\UnusedColumn;

class Header extends CcamEntry
{
    protected function getAlias(): string
    {
        return 'Entête';
    }

    protected function configure(): void
    {
        $this->columns[] = new StringColumn('typeEmetteur', 2);
        $this->columns[] = new StringColumn('numeroEmetteur', 14);
        $this->columns[] = new StringColumn('programmeEmetteur', 6);
        $this->columns[] = new StringColumn('typeDestinataire', 2);
        $this->columns[] = new StringColumn('numeroDestinataire', 14);
        $this->columns[] = new StringColumn('programmeDestinataire', 6);
        $this->columns[] = new StringColumn('applicationTypeEchange', 2);
        $this->columns[] = new StringColumn('identificationFichier', 3);
        $this->columns[] = new StringColumn('dateCreationFichier', 8);
        $this->columns[] = new UnusedColumn(24);
        $this->columns[] = new StringColumn('numeroChronologie', 5);
        $this->columns[] = new StringColumn('typeFichier', 1);
        $this->columns[] = new StringColumn('numeroVersion', 4);
        $this->columns[] = new UnusedColumn(34);
    }
}
