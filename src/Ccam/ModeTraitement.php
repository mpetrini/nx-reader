<?php

namespace NXReader\Ccam;

use NXReader\Entry\DateColumn;
use NXReader\Entry\IntColumn;
use NXReader\Entry\UnusedColumn;

class ModeTraitement extends CcamEntry
{
    protected function getAlias(): string
    {
        return 'TB01';
    }

    protected function configure(): void
    {
        $this->columns[] = new IntColumn('rubrique', 2);
        $this->columns[] = new IntColumn('sequence', 2);
        for ($ind = 1; $ind < 7; $ind++) {
            $this->columns[] = new IntColumn('modeTraitement'.$ind, 2);
            $this->columns[] = new DateColumn('dateDebut'.$ind, 8, 'SYmd');
            $this->columns[] = new DateColumn('dateFin'.$ind, 8, 'SYmd');
        }
        $this->columns[] = new UnusedColumn(13);
    }
}
