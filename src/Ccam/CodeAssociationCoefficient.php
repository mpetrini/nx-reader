<?php

namespace NXReader\Ccam;

use NXReader\Entry\DateColumn;
use NXReader\Entry\IntColumn;
use NXReader\Entry\StringColumn;
use NXReader\Entry\UnusedColumn;

class CodeAssociationCoefficient extends TarifCoefficient
{
    protected function getAlias(): string
    {
        return 'TB02';
    }
}
