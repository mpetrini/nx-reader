<?php

namespace NXReader\Ccam;

use NXReader\Entry\DateColumn;
use NXReader\Entry\IntColumn;
use NXReader\Entry\StringColumn;
use NXReader\Entry\UnusedColumn;

abstract class TarifCoefficient extends CcamEntry
{
    protected function configure(): void
    {
        $this->columns[] = new IntColumn('rubrique', 2);
        $this->columns[] = new IntColumn('sequence', 2);
        for ($ind = 1; $ind < 6; $ind++) {
            $this->columns[] = new IntColumn('grilleTarifaire'.$ind, 2);
            $this->columns[] = new StringColumn('codeAssociation'.$ind, 1);
            $this->columns[] = new DateColumn('dateDebut'.$ind, 8, 'SYmd');
            $this->columns[] = new DateColumn('dateFin'.$ind, 8, 'SYmd');
            $this->columns[] = new IntColumn('coefficient'.$ind, 4);
        }
        $this->columns[] = new UnusedColumn(1);
    }
}
