<?php

namespace NXReader\Extractor;

interface ExtractorInterface
{
    public function getFileUnzipped(): ?string;
}
