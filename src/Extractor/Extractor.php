<?php

namespace NXReader\Extractor;

use NXReader\Request\HttpRequest;
use NXReader\FileLoader\FileLoaderInterface;
use Symfony\Component\HttpFoundation\Response;
use ZipArchive;

class Extractor implements ExtractorInterface
{
    protected FileLoaderInterface $fileLoader;

    protected HttpRequest $httpRequest;

    public function __construct(
        FileLoaderInterface $fileLoader,
        HttpRequest $httpRequest
    ) {
        $this->fileLoader = $fileLoader;
        $this->httpRequest = $httpRequest;
    }

    protected function getPath(): string
    {
        return sys_get_temp_dir().'/dbf_reader.zip';
    }

    protected function getPathUnzipped(): string
    {
        return sys_get_temp_dir().'/dbf_reader-unzip';
    }

    public function __destruct()
    {
        if (is_file($this->getPath())) {
            unlink($this->getPath());
        }
        if (is_file($this->getPathUnzipped())) {
            unlink($this->getPathUnzipped());
        }
    }

    public function unzip(string $file): ?string
    {
        $bufferSize = 4096;
        $zip = new ZipArchive();
        $feed = $zip->open($file);

        if ($feed !== true) {
            return null;
        }

        $extracted = $zip->extractTo($this->getPathUnzipped());

        $zip->close();

        $defaultPath = $this->getPathUnzipped();
        $fileZipped = glob($defaultPath.'/*.gz')[0];
        $out_file_name = str_replace('.gz', '.txt', $fileZipped);

        $file = gzopen($fileZipped, 'rb');
        $out_file = fopen($out_file_name, 'wb');

        while (!gzeof($file)) {
            fwrite($out_file, gzread($file, $bufferSize));
        }

        fclose($out_file);
        gzclose($file);

        return $extracted === true ?
            glob($this->getPathUnzipped().'/*.txt')[0] :
            null;
    }

    public function getFile(): ?string
    {
        $this->httpRequest->init();
        $this->httpRequest->setOption(CURLOPT_URL, $this->fileLoader->getUrl());
        $this->httpRequest->setOption(CURLOPT_RETURNTRANSFER, true);

        $body = $this->httpRequest->execute();
        $httpCode = $this->httpRequest->getInfo(CURLINFO_HTTP_CODE);
        $this->httpRequest->close();

        if ($httpCode !== Response::HTTP_OK) {
            return null;
        }

        $file = fopen($this->getPath(), 'w');
        fwrite($file, $body);
        fclose($file);

        return $this->getPath();
    }

    public function getFileUnzipped(): ?string
    {
        $file = $this->getFile();

        if (!$file) {
            return null;
        }

        return $this->unzip($file);
    }
}
