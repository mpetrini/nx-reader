<?php

namespace NXReader\Reader;

class ReaderResource
{
    /**
     * @var ?resource
     */
    private $handle = null;

    public function open(string $filePath): bool
    {
        $handle = fopen($filePath, 'r');

        if (!$handle) {
            return false;
        }

        $this->handle = $handle;

        return true;
    }

    public function getNextData(): ?string
    {
        if (!$this->handle) {
            return null;
        }

        return fgets($this->handle);
    }
}
