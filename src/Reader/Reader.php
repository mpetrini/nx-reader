<?php

namespace NXReader\Reader;

use NXReader\Entry\Entry;
use NXReader\Proxy\ProxyInterface;

class Reader
{
    private ProxyInterface $proxy;

    private ReaderResource $readerResource;

    /**
     * @var Entry[]
     */
    private array $entries;

    public function __construct(
        ProxyInterface $proxy,
        ReaderResource $readerResource
    ) {
        $this->proxy = $proxy;
        $this->readerResource = $readerResource;
        $this->entries = [];
    }

    public function nextRecord(): ?Entry
    {
        $data = $this->readerResource->getNextData();
        $entryClassname = $this->proxy->redirect(
            substr($data, 0, $this->proxy->getLengthDiscrimator())
        );

        if (empty($entryClassname)) {
            return null;
        }

        /** @var Entry $entry */
        $entry = new $entryClassname(substr($data, $this->proxy->getLengthDiscrimator()));
        $entry->read();

        return $entry;
    }
}
