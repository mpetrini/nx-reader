<?php

namespace NXReader\Proxy;

interface ProxyInterface
{
    public function getLengthDiscrimator(): int;

    /**
     * @return class-string|null
     */
    public function redirect(string $recordingType): ?string;
}
