<?php

namespace NXReader\Proxy;

use NXReader\Ccam\CodeAssociationCoefficient;
use NXReader\Ccam\Header;
use NXReader\Ccam\ModeTraitement;
use NXReader\Ccam\RegleTarifaireCoefficient;
use NXReader\Ccam\RegroupementSpecialite;

class CcamProxy implements ProxyInterface
{
    public function getLengthDiscrimator(): int
    {
        return 3;
    }

    /**
     * {@inheritDoc}
     */
    public function redirect(string $recordingType): ?string
    {
        switch ($recordingType) {
            case '000':
                return Header::class;
            case '001':
                return ModeTraitement::class;
            case '002':
                return CodeAssociationCoefficient::class;
            case '003':
                return RegleTarifaireCoefficient::class;
            case '004':
                return RegroupementSpecialite::class;
            default:
                return null;
        }
    }
}
