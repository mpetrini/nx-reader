<?php

namespace NXReader\Entry;

class UnusedColumn extends Column
{
    public function __construct(int $length) {
        parent::__construct('unused', $length, 'string');
    }
}
