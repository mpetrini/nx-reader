<?php

namespace NXReader\Entry;

class StringColumn extends Column
{
    public function __construct(string $name, int $length) {
        parent::__construct($name, $length);
    }
}
