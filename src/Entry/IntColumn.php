<?php

namespace NXReader\Entry;

class IntColumn extends Column
{
    public function __construct(string $name, int $length) {
        parent::__construct($name, $length);
    }

    public function postProcess(string $value)
    {
        return intval(parent::postProcess($value));
    }
}
