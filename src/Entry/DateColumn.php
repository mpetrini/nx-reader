<?php

namespace NXReader\Entry;

use DateTimeImmutable;

class DateColumn extends Column
{
    private string $format;

    public function __construct(string $name, int $length, string $format)
    {
        parent::__construct($name, $length);
        $this->format = $format;
    }

    public function postProcess(string $value): ?DateTimeImmutable
    {
        if (empty($value)) {
            return null;
        }

        $value = DateTimeImmutable::createFromFormat($this->format, $value);

        return $value ?
            $value :
            null;
    }
}
