<?php

namespace NXReader\Entry;

class Column
{
    protected string $name;

    protected int $length;

    public function __construct(string $name, int $length) {
        $this->name = $name;
        $this->length = $length;
    }

    /**
     * @return mixed
     */
    public function postProcess(string $value)
    {
        return $value;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLength(): int
    {
        return $this->length;
    }
}
