<?php

namespace NXReader\Entry;

abstract class Entry
{
    private string $data = '';

    /**
     * @var mixed[]
     */
    private array $values = [];

    /**
     * @var Column[]
     */
    protected array $columns = [];

    final public function __construct(string $data)
    {
        $this->data = $data;
    }

    abstract protected function configure(): void;

    public function read(): array
    {
        $this->configure();
        $this->values = [];

        $index = 0;
        foreach ($this->columns as $column) {
            $this->values[$column->getName()] = $column->postProcess(
                substr($this->data, $index, $column->getLength())
            );

            $index += $column->getLength();
        }

        return $this->values;
    }
}
